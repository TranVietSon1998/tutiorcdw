package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.test.entities.Customer;
import com.test.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	private final CustomerService customerService;

	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@RequestMapping(value={"/"})
	public String listCustomer(Model model) {
		model.addAttribute("listCustomer", customerService.findAll());
		return "index";
	}



///*	@RequestMapping(value = "/insert")
//	public Customer create(@RequestBody Customer customer) {
//		return customerService.save(customer);
//	}
//
//	@RequestMapping(value = "/findByID/{id}")
//	public Customer findById(@PathVariable(value = "id") String id) {
//		return customerService.findById(Integer.valueOf(id));
//	}
//
//	@RequestMapping(value = "/update")
//	public Customer update(@RequestBody Customer customer) {
//		return customerService.update(customer);
//	}
//
//	@RequestMapping(value = "/delete/{id}")
//	public void delete(@PathVariable(value = "id") String id) {
//		customerService.delete(Integer.valueOf(id));
//	}*/



}
